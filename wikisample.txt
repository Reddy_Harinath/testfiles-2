||heading 1||heading 2||heading 3||
|*cell A1*|cell A2|cell A3|
|cell B1|cell B2|cell B3|

|| || January || February || March || April ||
| Max | 37.5 | 32.7 | 28.0 | 25.3 |
| Min | 31.3 | 26.8 | 25.1 | 18.7 |

||Heading 1||Heading 2||
|* Item 1
* Item 2
* Item 3|# Item 1
# Item 2
# Item 3|

# Here
#* is
#* an
# example
#* of
#* a
# mixed
# list